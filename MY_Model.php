<?php

class MY_Model extends CI_Model{
    /**
     * 外部キーで参照しているテーブルに関する情報(モデル名,主キー名,外部キー名)
     *
     * @var array
     */
    protected $belongs_to = array();

    /**
     * テーブル名
     *
     * @var string
     */
    protected $table_name = '';

    /**
     * データベースオブジェクト
     *
     * @var object
     */
    protected $builder;

    /**
     * コンストラクタ
     * 
     * @return void
     */
    public function __construct() {
        parent::__construct();
        $this->builder = $this->load->database('', true);
    }

    /**
     * テーブル名をセット
     *
     * @return void
     */
    protected function _set_from(){
        $this->builder->start_cache();
        $this->builder->from($this->table_name);
        $this->builder->stop_cache();
    }

    /**
     * データベースオブジェクトのメソッド呼び出し
     *
     * @param string $name
     * @param array $arguments
     * @return mixed
     */
    public function __call($name, $arguments){
        return call_user_func_array([$this->builder, $name], $arguments);
    }

    /**
     * テーブル名取得
     *
     * @return string
     */
    public function get_table_name(){
        return $this->table_name;
    }

    /**
     * 外部キーで参照するテーブル名と結合条件の配列を取得
     *
     * @return array
     */
    public function get_belongs_to_chain($recursive = true){
        $chain = array();
        foreach($this->belongs_to as $belongs_to){
            $parent_model = $belongs_to['model'];
            $this->load->model($parent_model);
            $primary_key = $belongs_to['primary_key'];
            $foreign_key = $belongs_to['foreign_key'];
            $parent_table_name = $this->{$parent_model}->get_table_name();
            $chain[] = array(
                'table_name' => $parent_table_name,
                'on' => "{$parent_table_name}.{$primary_key} = {$this->table_name}.{$foreign_key}",
            );
            if($recursive){
                $chain = array_merge($chain, $this->{$parent_model}->get_belongs_to_chain($recursive));
            }
        }
        return $chain;
    }
}